﻿using UnityEngine;

public class TrackerSoundPlayer : BaseBehavior
{
    public Animator animator;
    public GameObject body;

    [HideInInspector]
    public bool inSlot = false;

    [SerializeField]
	[FMODUnity.EventRef]
	public string soundEvent;
    
    protected override void Awake()
    {
        base.Awake();
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("Playhead") && inSlot)
        {
            // if (SoundManager.Instance != null)
            // {
            //     SoundManager.Instance.Play(soundEvent, gameObject);
            // }

            if (this.body != null)
            {
                LeanTween.rotateLocal(this.body, Vector3.zero, 1f).setEase(LeanTweenType.easeOutElastic).setFrom(new Vector3(0, 0, 30f));
                LeanTween.scale(this.body, Vector3.one, 1f).setEase(LeanTweenType.easeOutElastic).setFrom(new Vector3(1.2f, 1.2f, 1f));
            }

            if (this.animator != null)
            {
                this.setTrigger(this.animator, "play");
            }
        }
	}
}

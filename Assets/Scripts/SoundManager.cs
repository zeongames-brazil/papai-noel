﻿using System.Collections.Generic;
using UnityEngine;

public class SoundManager : Singleton<SoundManager>
{
	[SerializeField]
	[FMODUnity.EventRef]
	private List<string> _soundEvents = new List<string>();

	private Dictionary<string, FMOD.Studio.EventInstance> _audioList =  new Dictionary<string, FMOD.Studio.EventInstance>();

	void Awake()
	{
		for (int i = 0; i < _soundEvents.Count; i++) {
			_audioList.Add(_soundEvents[i], FMODUnity.RuntimeManager.CreateInstance(_soundEvents[i]));
		}
	}

	public void Play(string soundEvent, GameObject gameObject, float volume)
	{
		if (_audioList.ContainsKey(soundEvent)) {
			FMOD.ATTRIBUTES_3D attr3D = FMODUnity.RuntimeUtils.To3DAttributes(gameObject);

			_audioList[soundEvent].set3DAttributes(attr3D);
			_audioList[soundEvent].setVolume(volume);
			_audioList[soundEvent].start();
		}
	}

	public void Play(string soundEvent, GameObject gameObject)
	{
		Play(soundEvent, gameObject, 1f);
	}
}

﻿using UnityEngine;
using System.Collections;

namespace ZeonStudios.Debug {
	public class CameraMouseRotation : MonoBehaviour
	{
	#if UNITY_EDITOR
		private Transform camera;
		private Vector3 angle;

		[Range(0.5f,3)]
		[SerializeField]
		private float velocityX = 1;
		[Range(0.5f,3)]
		[SerializeField]
		private float velocityY = 1;

		void Start ()
		{
			camera = this.transform;
			angle = camera.localEulerAngles;
		}

		void Update ()
		{
			if (!Input.GetKey(KeyCode.LeftAlt)) {
				return;
			}

			angle.y += Input.GetAxisRaw("Mouse X") * velocityX;
			angle.x -= Input.GetAxisRaw("Mouse Y") * velocityY;

			camera.localEulerAngles = angle;
		}
	#endif
	}
}

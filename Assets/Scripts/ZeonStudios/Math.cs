﻿using UnityEngine;

namespace ZeonStudios {
	public static class Math
	{
		public static Vector3 ProjectPointOnLine(Vector3 linePoint, Vector3 lineVec, Vector3 point)
		{		
			Vector3 linePointToPoint = point - linePoint;
	 
			float t = Vector3.Dot(linePointToPoint, lineVec);
	 
			return linePoint + lineVec * t;
		}
 
		public static Vector3 ProjectPointOnLineSegment(Vector3 linePoint1, Vector3 linePoint2, Vector3 point)
		{
			Vector3 vector = linePoint2 - linePoint1;
			Vector3 projectedPoint = ProjectPointOnLine(linePoint1, vector.normalized, point);
			Vector3 pointVec = point - linePoint1;

			float dot = Vector3.Dot(pointVec, vector);

			if(dot > 0) {
				if (pointVec.magnitude <= vector.magnitude) {
					return projectedPoint;
				}
				return linePoint2;
			}
			return linePoint1;
		}
	}
}

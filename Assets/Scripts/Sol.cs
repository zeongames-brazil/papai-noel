﻿using UnityEngine;

public class Sol : Singleton<Sol>
{
    public Animator animator;

    public GameObject body;

    private Vector3 velocity;

    private Vector3 target;

    private int total;

    protected override void Awake()
    {
        base.Awake();
    }

    private void Start()
    {
        LeanTween.moveLocal(this.body, Vector3.up * 5f, 4f).setLoopPingPong().setEaseInOutSine();
        this.target = this.transform.position;
    }

    private void Update()
    {
        this.transform.forward = this.transform.position.normalized;

        Vector3 direction = this.transform.position;
        direction.y = 0;

        Vector3 playerDirection = Camera.main.transform.forward;
        playerDirection.y = 0;
        playerDirection.Normalize();

        if (Vector3.Dot(playerDirection, direction.normalized) < 0.666f)
        {
            this.target = playerDirection * 50f + Vector3.up * 40f;
        }

        if (this.transform.position != this.target)
        {
            this.transform.position = Vector3.SmoothDamp(this.transform.position, this.target, ref velocity, 3f);
        }

        if (this.total != SlotProperties.total)
        {
            this.total = SlotProperties.total;

            if (this.total >= 6)
            {
                this.play();
            }
            else
            {
                this.stop();
            }
        }
    }

    public void play()
    {
        this.setTrigger(this.animator, "play");
    }

    public void stop()
    {
        this.setTrigger(this.animator, "stop");
    }
}
﻿using UnityEngine;

public static class ParticleExtension
{
    public static void setEmissionState(this ParticleSystem p, bool state)
    {
        if (state != p.emission.enabled)
        {
            var emission = p.emission;
            emission.enabled = state;

            var list = p.GetComponentsInChildren<ParticleSystem>();
            
            for (int i = 0; i < list.Length; i++)
            {
                emission = list[i].emission;
                emission.enabled = state;
            }
            
        }
    }

    public static bool getEmissionState(this ParticleSystem p)
    {
        return p.emission.enabled;
    }
}
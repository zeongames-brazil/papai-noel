﻿using UnityEngine;

public class FixedPosition : BaseBehavior
{
    public LayerMask groundLayer;

    private float height = 0;

    protected override void Awake()
    {
        base.Awake();
    }

    private void Update()
    {
        Vector3 pos = this.transform.parent.position;
        pos.y = this.height;

        this.transform.position = pos;
        this.transform.rotation = Quaternion.LookRotation(this.transform.position.normalized, Vector3.up);
    }

    private void FixedUpdate()
    {
        Vector3 position = this.transform.position;
        position.y = 0;

        Ray ray = new Ray(position + Vector3.up * 2f, Vector3.down);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 5f, this.groundLayer))
        {
            height = hit.point.y + 0.001f;
        }
    }
}
﻿using UnityEngine;

/// <summary>
/// Drag a rigidbody with the mouse using a spring joint.
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class DragRigidbody : BaseBehavior, IGvrGazeResponder
{
    public LayerMask interactiveLayer;
    public LayerMask slotLayer;

    public GameObject body;

    public float force = 600;
    public float damping = 6;

    private float waitTimer = 0;
    private float particleTimer = 0;
    private float inputTimer = 0;

    [HideInInspector]
    public Transform jointTrans;
    private float dragDepth;

    private SlotProperties properties;

    private bool isSelected = false;
    private bool isDragging = false;

    private TrackerSoundPlayer trackerSoundPlayer;

    protected override void Awake()
    {
        base.Awake();
        this.trackerSoundPlayer = this.GetComponent<TrackerSoundPlayer>();
    }
    
    private void Update()
    {
        if (this.waitTimer > 0)
        {
            this.waitTimer -= Time.deltaTime;
        }
        if (this.particleTimer > 0)
        {
            this.particleTimer -= Time.deltaTime;
        }
        if (this.inputTimer > 0)
        {
            this.inputTimer -= Time.deltaTime;
        }

#if UNITY_ANDROID
        if (Input.GetMouseButtonDown(0))
#elif
        if (OVRInput.GetDown(OVRInput.Button.One))
#endif
        {
            if (this.inputTimer <= 0)
            {
                this.inputTimer = 0.3f;

                if (this.isDragging)
                {
                    this.HandleInputEnd();
                    this.isDragging = false;
                }
                else if (!this.isDragging && this.isSelected)
                {
                    this.HandleInputBegin();
                    this.isDragging = true;
                }
            }
        }
    }

    private void FixedUpdate()
    {
        Vector3 direction = this.transform.position;
        direction.y = 0;

        if (this.isDragging)
        {
            this.HandleInput();
        }
        else
        {
            this.transform.forward = direction.normalized;
        }

        if (this.transform.position.y < -5f && this.waitTimer <= 0)
        {
            SoundManager.Instance.Play("event:/sounds/gandula", gameObject);

            this.waitTimer = 2f;

            LeanTween.cancel(this.gameObject);
            this.body.transform.localRotation = Quaternion.identity;
            this.gameObject.layer = LayerMask.NameToLayer("Interactive");

            if (this.properties != null)
            {
                this.properties.RemoveSlot(this.guid);
                trackerSoundPlayer.inSlot = false;
                this.properties = null;
            }

            if (this.jointTrans != null)
            {
                Destroy(jointTrans.gameObject);
                this.rigidbody.MovePosition(this.transform.position);
                jointTrans = null;
            }

            this.transform.forward = direction.normalized;
            this.rigidbody.position = direction.normalized * 15f + Vector3.up * 3f;

            this.rigidbody.velocity = Vector3.zero;
            this.rigidbody.angularVelocity = Vector3.zero;

            this.rigidbody.AddForce(-direction.normalized * Random.Range(180f, 190f) + Vector3.up * Random.Range(450f, 455f));
        }
    }
    
    private void OnCollisionEnter(Collision collision)
    {
        if (this.particleTimer <= 0)
        {
            this.particleTimer = 0.2f;

            SoundManager.Instance.Play("event:/sounds/drop", this.gameObject);

            if (Vector3.Dot(Vector3.up, collision.contacts[0].normal) > 0.8f)
            {
                ParticleManager.Instance.playImpact(this.transform.position, collision.contacts[0].normal);
            }
            else
            {
                ParticleManager.Instance.playImpact(this.collider.bounds.center, collision.contacts[0].normal);
            }
        }
    }
    
    private void HandleInputBegin()
    {
        LeanTween.cancel(this.gameObject);
        this.body.transform.localRotation = Quaternion.identity;
        this.gameObject.layer = LayerMask.NameToLayer("Interactive");

        if (jointTrans != null)
        {
            Destroy(jointTrans.gameObject);
            this.rigidbody.MovePosition(this.transform.position);
            jointTrans = null;
        }

        Vector3 direction = Camera.main.transform.position - this.transform.position;
        direction.y = 0;
        this.transform.forward = -direction.normalized;

        Ray ray = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 15f, this.interactiveLayer))
        {
            jointTrans = AttachJoint(this.rigidbody, this.transform.position);
            dragDepth = 5f;
        }
        if (trackerSoundPlayer.inSlot)
        {
            if (Physics.Raycast(ray, out hit, 15f, this.slotLayer))
            {
                SlotProperties slotProperties = hit.collider.gameObject.GetComponent<SlotProperties>();

                slotProperties.RemoveSlot(guid);
                trackerSoundPlayer.inSlot = false;
            }   
        }
    }

    private void HandleInput()
    {
        if (jointTrans == null)
            return;

        Vector3 direction = Camera.main.transform.forward;
        direction.y = 0;

        jointTrans.forward = direction.normalized;
        jointTrans.position = Camera.main.transform.position + Camera.main.transform.forward * dragDepth;
    }

    private void HandleInputEnd()
    {
        if (jointTrans != null)
        {
            LeanTween.cancel(this.gameObject);
            this.body.transform.localRotation = Quaternion.identity;
            this.gameObject.layer = LayerMask.NameToLayer("Interactive");

            Destroy(jointTrans.gameObject);
            this.rigidbody.MovePosition(this.transform.position);
            jointTrans = null;
            
            Ray ray = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 15f, this.slotLayer))
            {
                SlotProperties slotProperties = hit.collider.gameObject.GetComponent<SlotProperties>();
                Vector3 slotPosition;
                if (slotProperties.SlotInsert(gameObject, out slotPosition))
                {
                    this.properties = slotProperties;
                    this.transform.forward = hit.transform.forward;
                    jointTrans = AttachJoint(this.rigidbody, this.transform.position);
                    jointTrans.transform.position = slotPosition;
                    trackerSoundPlayer.inSlot = true;
                }
                else
                {
                    trackerSoundPlayer.inSlot = false;
                }
            }
        }
    }

    private Transform AttachJoint(Rigidbody rb, Vector3 attachmentPosition)
    {
        GameObject go = new GameObject("Attachment Point");
        go.hideFlags = HideFlags.HideInHierarchy;
        go.transform.position = attachmentPosition;

        Vector3 direction = Camera.main.transform.forward;
        direction.y = 0;

        go.transform.forward = direction.normalized;

        var newRb = go.AddComponent<Rigidbody>();
        newRb.isKinematic = true;

        var joint = go.AddComponent<ConfigurableJoint>();
        joint.connectedBody = rb;
        joint.configuredInWorldSpace = true;
        joint.xDrive = NewJointDrive(force, damping);
        joint.yDrive = NewJointDrive(force, damping);
        joint.zDrive = NewJointDrive(force, damping);
        joint.slerpDrive = NewJointDrive(force, damping);
        joint.rotationDriveMode = RotationDriveMode.Slerp;

        this.gameObject.layer = LayerMask.NameToLayer("IgnoreCollider");

        return go.transform;
    }

    private JointDrive NewJointDrive(float force, float damping)
    {
        JointDrive drive = new JointDrive();
        drive.positionSpring = force;
        drive.positionDamper = damping;
        drive.maximumForce = Mathf.Infinity;
        return drive;
    }

    public void OnGazeEnter()
    {
        if (!this.isSelected && this.body != null)
        {
            LeanTween.scale(this.body, new Vector3(1.05f, 1.05f, 1f), 0.1f).setEase(LeanTweenType.easeInCubic);
        }

        this.isSelected = true;
    }

    public void OnGazeExit()
    {
        if (this.isSelected && this.body != null)
        {
            LeanTween.scale(this.body, Vector3.one, 0.1f).setEase(LeanTweenType.easeInCubic);
        }

        this.isSelected = false;
    }

    public void OnGazeTrigger()
    {
        
    }
}
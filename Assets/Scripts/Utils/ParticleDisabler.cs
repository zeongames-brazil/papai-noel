﻿using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleDisabler : MonoBehaviour
{
    public bool activateOnPlay = true;

    public bool destroyObject = true;

    private ParticleSystem particles;
    
    private float timer = 0f;

    private void Start()
    {
        particles = this.GetComponent<ParticleSystem>();
        
        if (activateOnPlay)
        {
            particles.Play(true);
        }
    }

    private void OnEnable()
    {
        this.timer = 0f;
    }

    private void Update()
    {
        if (!particles.IsAlive(true) && particles.isStopped)
        {
            timer += Time.deltaTime;

            if (timer >= particles.main.duration)
            {
                if (this.destroyObject)
                {
                    GameObject.Destroy(this.gameObject);
                }
                else
                {
                    this.gameObject.SetActiveCustom(false);
                }
            }
        }
    }
}

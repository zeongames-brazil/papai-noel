﻿using UnityEngine;

public static class GameObjectExtension
{
    public static void SetActiveCustom(this GameObject obj, bool value)
    {
        if (value != obj.activeSelf)
        {
            obj.SetActive(value);
        }
    }
}
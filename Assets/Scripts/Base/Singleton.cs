﻿using UnityEngine;

public class Singleton<T> : BaseBehavior where T : BaseBehavior
{
    private static T instance;

    //Returns the instance of this singleton.
    public static T Instance
    {
        get
        {
            if (instance == null)
            {
                instance = (T)FindObjectOfType(typeof(T));
            }

            return instance;
        }
    }
}
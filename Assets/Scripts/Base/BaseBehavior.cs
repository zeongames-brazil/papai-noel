﻿using MovementEffects;
using System.Collections.Generic;
using UnityEngine;

public class BaseBehavior : MonoBehaviour
{
    private Transform _transform;
    private Rigidbody _rigidbody;
    private Collider _collider;

    [HideInInspector]
    public new Transform transform
    {
        get
        {
            if (this._transform == null)
            {
                this._transform = this.GetComponent<Transform>();
            }
            return this._transform;
        }
    }

    [HideInInspector]
    public new Rigidbody rigidbody
    {
        get
        {
            if (this._rigidbody == null)
            {
                this._rigidbody = this.GetComponent<Rigidbody>();
            }
            return this._rigidbody;
        }
    }

    [HideInInspector]
    public new Collider collider
    {
        get
        {
            if (this._collider == null)
            {
                this._collider = this.GetComponent<Collider>();
            }
            return this._collider;
        }
    }

    [HideInInspector]
    public string guid;

    protected virtual void Awake()
    {
        this.guid = System.Guid.NewGuid().ToString();
    }

    protected void setTrigger(Animator animator, string trigger)
    {
        if (this.gameObject.activeInHierarchy)
        {
            Timing.RunCoroutine(this._setTrigger(animator, trigger), this.guid);
        }
    }

    private IEnumerator<float> _setTrigger(Animator animator, string trigger)
    {
        while (!animator.isInitialized)
        {
            yield return Timing.WaitForSeconds(0.1f);
        }

        animator.SetTrigger(trigger);
    }

    protected void setFloat(Animator animator, string trigger, float value)
    {
        if (this.gameObject.activeInHierarchy)
        {
            Timing.RunCoroutine(this._setFloat(animator, trigger, value), this.guid);
        }
    }

    private IEnumerator<float> _setFloat(Animator animator, string trigger, float value)
    {
        while (!animator.isInitialized)
        {
            yield return Timing.WaitForSeconds(0.1f);
        }

        animator.SetFloat(trigger, value);
    }

    protected void setBool(Animator animator, string trigger, bool flag)
    {
        if (this.gameObject.activeInHierarchy)
        {
            Timing.RunCoroutine(this._setBool(animator, trigger, flag), this.guid);
        }
    }

    private IEnumerator<float> _setBool(Animator animator, string trigger, bool flag)
    {
        while (!animator.isInitialized)
        {
            yield return Timing.WaitForSeconds(0.1f);
        }

        animator.SetBool(trigger, flag);
    }

    protected void resetTrigger(Animator animator, string trigger)
    {
        if (this.gameObject.activeInHierarchy)
        {
            Timing.RunCoroutine(this._resetTrigger(animator, trigger), this.guid);
        }
    }

    private IEnumerator<float> _resetTrigger(Animator animator, string trigger)
    {
        while (!animator.isInitialized)
        {
            yield return Timing.WaitForSeconds(0.1f);
        }

        animator.ResetTrigger(trigger);
    }

    protected void killCoroutines()
    {
        Timing.KillCoroutines(this.guid);
    }
}
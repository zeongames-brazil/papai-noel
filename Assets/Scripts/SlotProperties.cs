﻿using UnityEngine;
using System.Collections.Generic;

public class SlotProperties : BaseBehavior
{
    [HideInInspector]
	public bool attachedGameObject = false;

	[SerializeField]
	private Transform[] slots;

    private int count = 0;
    private int _count = 0;

	private struct SlotData
	{
		public Vector3 position;
		public GameObject gameObject;
		public DragRigidbody drag;
		public TrackerSoundPlayer sound;
	}

	private List<SlotData> _slotList = new List<SlotData>();

	[HideInInspector]
	public static int total;

	protected override void Awake()
	{
		base.Awake();

		for (int i = 0; i < slots.Length; i++)
		{
			SlotData auxSlot;
			auxSlot.position   = slots[i].position;
			auxSlot.gameObject = null;
			auxSlot.drag       = null;
			auxSlot.sound      = null;

			_slotList.Add(auxSlot);
		}
	}

    private void FixedUpdate()
    {
        if (this.count != this._count)
        {
            this._count = this.count;

            this.ReorderSlotList();
        }
    }

    public bool SlotInsert(GameObject gameObject, out Vector3 position)
	{
		position = Vector3.zero;

		for (int i = 0; i < _slotList.Count; i++)
		{
			if (_slotList[i].gameObject == null)
			{
				SlotData auxSlot;
				auxSlot.position   = _slotList[i].position;
				auxSlot.gameObject = gameObject;
				auxSlot.drag       = gameObject.GetComponent<DragRigidbody>();
				auxSlot.sound      = gameObject.GetComponent<TrackerSoundPlayer>();

				_slotList[i] = auxSlot;

                this.count++;
                total++;

				position = _slotList[i].position;
				return true;
			}
		}

		return false;

	}

	public void RemoveSlot(string guid)
	{
		for (int i = 0; i < _slotList.Count; i++)
		{
			if (_slotList[i].drag != null && _slotList[i].drag.guid == guid)
			{
				SlotData auxSlot;
				auxSlot.position   = _slotList[i].position;
				auxSlot.gameObject = null;
				auxSlot.drag       = null;
				auxSlot.sound      = null;

				_slotList[i] = auxSlot;

                this.count--;
                total--;

				return;
			}
		}
	}

	private void ReorderSlotList()
	{
		if (_slotList.Count <= 1)
		{
			return;
		}
		for (int i = 0; i < _slotList.Count; i++)
		{
			for (int j = i+1; j < _slotList.Count; j++)
			{
				if (_slotList[i].gameObject == null && _slotList[j].gameObject != null)
				{
					SwapPosition(j, i);
				}
			}
		}
	}

	private void SwapPosition(int indexFrom, int indexTo)
	{
		_slotList[indexFrom].drag.jointTrans.position = _slotList[indexTo].position;

		SlotData auxSlot;
		auxSlot.position   = _slotList[indexTo].position;
		auxSlot.drag       = _slotList[indexFrom].drag;
		auxSlot.sound      = _slotList[indexFrom].sound;
		auxSlot.gameObject = _slotList[indexFrom].gameObject;

		_slotList[indexTo] = auxSlot;

		auxSlot.position   = _slotList[indexFrom].position;
		auxSlot.drag       = null;
		auxSlot.gameObject = null;
		auxSlot.sound      = null;

		_slotList[indexFrom] = auxSlot;
	}

	public void PlaySlots()
	{
		for (int i = 0; i < _slotList.Count; i++)
		{
			if (_slotList[i].sound != null)
			{
				SoundManager.Instance.Play(_slotList[i].sound.soundEvent, _slotList[i].gameObject);
			}
		}
	}
}

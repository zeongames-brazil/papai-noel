﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMOD;
using FMODUnity;
using System;
using System.Runtime.InteropServices;

public class BeatDetection : Singleton<BeatDetection> 
{
    [EventRef]
    public string _musicEventName;

    FMOD.Studio.EventInstance _musicInstance;
    FMOD.Studio.EventDescription _musicEventDescription;
    FMOD.Studio.EVENT_CALLBACK _beatCallback;

    FMOD.Studio.EventInstance _auxInstance;

    FMOD.Studio.System __studioSystem;
    FMOD.Studio.CPU_USAGE cpuUsage;

    public List<SlotProperties> trackerSlots = new List<SlotProperties>();

    [HideInInspector]
    public int beatCounter = 1;

    public GameObject tracker;
	
	void Start ()
	{
        __studioSystem = RuntimeManager.StudioSystem;

        _beatCallback = new FMOD.Studio.EVENT_CALLBACK(BeatEventCallback);

        __studioSystem.getEvent(_musicEventName, out _musicEventDescription);
        _musicEventDescription.createInstance(out _musicInstance);

        _musicInstance.setCallback(_beatCallback, FMOD.Studio.EVENT_CALLBACK_TYPE.TIMELINE_BEAT);

        _musicInstance.start();
        _musicInstance.setVolume(0f);
    }

    [AOT.MonoPInvokeCallback(typeof(FMOD.Studio.EVENT_CALLBACK))]
    private RESULT BeatEventCallback(FMOD.Studio.EVENT_CALLBACK_TYPE type, IntPtr instancePtr, IntPtr parameterPtr)
    {
        FMOD.Studio.EventInstance eventInstance = new FMOD.Studio.EventInstance(instancePtr);

        if (type == FMOD.Studio.EVENT_CALLBACK_TYPE.TIMELINE_BEAT)
        {
        	var parameter = (FMOD.Studio.TIMELINE_BEAT_PROPERTIES)Marshal.PtrToStructure(parameterPtr, typeof(FMOD.Studio.TIMELINE_BEAT_PROPERTIES));
        	beatCounter++;

        	if (beatCounter % 2 == 0)
        	{
        		int beatInt = (beatCounter / 2) % 16;
        		trackerSlots[beatInt].PlaySlots();
        		LeanTween.rotateY(tracker, (beatInt + 1) * 360/16 + 3, 0.5f);
        	}
        }   

        return RESULT.OK;
    }

    public void SetBeatVolume(float volume)
    {
    	_musicInstance.setVolume(volume);
    }
}
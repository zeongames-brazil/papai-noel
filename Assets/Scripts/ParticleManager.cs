﻿using UnityEngine;

public class ParticleManager : Singleton<ParticleManager>
{
    public GameObject impactParticles;

    protected override void Awake()
    {
        base.Awake();
    }

    public void clear()
    {
        foreach (Transform child in this.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }
    
    public void playImpact(Vector3 position, Vector3 direction)
    {
        if (this.impactParticles != null)
        {
            GameObject particle = (GameObject)Instantiate(this.impactParticles, position, Quaternion.identity);
            particle.transform.parent = this.transform;
            particle.transform.forward = direction;
        }
    }
}
﻿using UnityEngine;

public class Nuvem : MonoBehaviour
{
    public Animator animator;

    public GameObject body;

    private void Start()
    {
        LeanTween.moveLocal(this.body, Vector3.up * 3f, 3f).setLoopPingPong().setEaseInOutSine();
    }

    private void Update()
    {
        this.transform.forward = this.transform.position.normalized;
    }
}
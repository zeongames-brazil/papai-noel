﻿using MovementEffects;
using System.Collections.Generic;
using UnityEngine;

public class PapaiNoel : BaseBehavior, IGvrGazeResponder
{
    public GameObject container;

    private List<DragRigidbody> list;

    private bool isOn = false;
    private bool initialized = false;

    public SpriteRenderer rendererOn;
	public SpriteRenderer rendererOff;

	private bool isSelected = false;

    protected override void Awake()
    {
        base.Awake();

        this.list = new List<DragRigidbody>(this.container.GetComponentsInChildren<DragRigidbody>(true));
    }

    public void OnGazeEnter()
    {
    	this.isSelected = true;
    }

    public void OnGazeExit()
    {
    	this.isSelected = false;
    }

    public void OnGazeTrigger()
    {
        
    }

    private void Update()
    {

#if UNITY_ANDROID
        if (Input.GetMouseButtonDown(0) && this.isSelected)
#elif
        if (OVRInput.GetDown(OVRInput.Button.One) && this.isSelected)
#endif
    	{
			isOn = !isOn;
	        if (isOn)
	        {
	        	BeatDetection.Instance.SetBeatVolume(1f);
	        	rendererOn.enabled  = true;
	        	rendererOff.enabled = false;
	        }
	        else
	        {
	        	BeatDetection.Instance.SetBeatVolume(0f);
	        	rendererOn.enabled  = false;
	        	rendererOff.enabled = true;
	        }

            if (this.isOn && !this.initialized)
            {
                this.initialized = true;

                Timing.RunCoroutine(this._spawn());
            }
    	}
    }

    private IEnumerator<float> _spawn()
    {
        Debug.Log(this.list.Count);
        while (this.list.Count > 0)
        {
            var obj = this.list[Random.Range(0, this.list.Count)];

            obj.gameObject.SetActiveCustom(true);

            this.list.Remove(obj);

            yield return Timing.WaitForSeconds(0.1f);
        }

        yield return 0;
    }
}
